// 引入@vueuse/core(验证码倒计时)
import { useIntersectionObserver, useIntervalFn } from '@vueuse/core'
import { ref } from 'vue'

// 封装倒计时函数
export const useCountDown = () => {
  const time = ref(0)
  const { pause, resume } = useIntervalFn(() => {
    time.value--
    if (time.value <= 0) {
      pause()
    }
  }, 1000, { immediate: false })

  const start = (sec) => {
    time.value = sec
    resume()
  }
  return { start, time }
}
/**
 * 功能：数据的懒加载
 * @param {*} fn 当目标可见时，需要调用一次函数
 * @returns target：要观察的目标元素（dom元素）
 */
export function useLazyData (fn) {
  const target = ref(null) // 把目标引用
  const { stop } = useIntersectionObserver(
    target, // target 是vue的对象引用。是观察的目标
    // isIntersecting 是否进入可视区域，true是进入 false是移出
    // observerElement 被观察的dom
    ([{ isIntersecting }], observerElement) => {
      // 在此处可根据isIntersecting来判断，然后做业务
      if (isIntersecting) {
        // 1. 停止观察
        stop()
        // 2. 发一次请求
        fn()
        // findHot().then(data => {
        //   goods.value = data.result
        // })
      }
    },
    // threshold为可视区域的比值，默认为1/10
    // 当前元素在页面中显示比值小于1/10时会出现白屏
    { threshold: 0 }
  )
  return target
}
