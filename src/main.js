import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 样式重置插件
import 'normalize.css'
// 样式重置
import '@/styles/common.less'

// 引入全局插件
import myPlugin from '@/components/index'

// 引入全局指令
import myDirective from '@/directives'

const app = createApp(App)

app.use(store).use(router).use(myPlugin).use(myDirective).mount('#app')
