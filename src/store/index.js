// createLogger为自带的vue3.0的调试工具
import { createStore, createLogger } from 'vuex'

import user from './modules/user'
import cart from './modules/cart'
import category from './modules/category'

// 引入持久化插件
import createPersistedstate from 'vuex-persistedstate'
export default createStore({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    user,
    cart,
    category
  },
  // 配置持久化插件
  plugins: [
    createPersistedstate({
      key: 'erabbit-client-pc-store',
      paths: ['user', 'cart']
    }),
    createLogger()
  ]
})
