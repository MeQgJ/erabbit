import { findHeadCategory } from '@/api/home'
// 分类模块
export default {
  namespaced: true,
  state () {
    return {
      // 分类信息集合
      list: []
    }
  },
  mutations: {
    // 将获取到的分类列表存储到state中
    setList (state, categoryList) {
      state.list = categoryList
    }
  },
  actions: {
    async getList (context) {
      // 发送请求获取数据
      try {
        const res = await findHeadCategory()
        context.commit('setList', res.result)
      } catch (err) {
        console.log(err)
      }
    }
  }
}
