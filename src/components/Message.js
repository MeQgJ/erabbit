// 导入.vue组件，导出一个函数，方便我们直接以函数的方式来调用组件

// 1. 导入组件
// ----- 显示 ------
// 2. 根据组件创建虚拟节点. const vnode = createVNode(XtxMessage, { type, text })
// 3. 准备一个DOM容器
// 4. 把虚拟节点渲染DOM容器中. render(vnode, div)
// -----隐藏DOM-----
// 5. 开启定时器，移出DOM容器内容
import { createVNode, render } from 'vue'
import XtxMessage from './xtx-message.vue'

// 准备dom容器
const div = document.createElement('div')
div.setAttribute('class', 'xtx-message-wrapper')
document.body.appendChild(div)
let timer = null
export default ({ type, text }) => {
  // 根据组件创建虚拟节点
  const vnode = createVNode(XtxMessage, { type, text })
  // 把虚拟节点渲染DOM容器中
  render(vnode, div)

  // 在1s后设置为空（清除虚拟dom中的内容）
  clearTimeout(timer)
  timer = setTimeout(() => {
    render(null, div)
  }, 2000)
}
