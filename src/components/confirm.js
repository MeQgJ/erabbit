// 1. 导入组件
// ----- 显示 ------
// 2. 根据组件创建虚拟节点. const vnode = createVNode(XtxMessage, { type, text })
// 3. 准备一个DOM容器
// 4. 把虚拟节点渲染DOM容器中. render(vnode, div)
// -----隐藏DOM-----
// 点击确定或取消都要隐藏Dom
import { createVNode, render } from 'vue'
import XtxConfirm from './xtx-confirm.vue'

// 准备dom容器
const div = document.createElement('div')
div.setAttribute('class', 'xtx-confirm-wrapper')
document.body.appendChild(div)

export default ({ title, text }) => {
  return new Promise((resolve, reject) => {
    // 点击确定，关闭组件
    const submitCallback = () => {
      render(null, div)
      resolve()
    }
    // 点击取消，关闭组件
    const cancelCallback = () => {
      render(null, div)
      reject(new Error('取消'))
    }
    const vnode = createVNode(XtxConfirm, { title, text, submitCallback, cancelCallback })
    render(vnode, div)
  })
}
