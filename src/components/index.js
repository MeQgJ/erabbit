// 以插件的格式定义全局组件
import XtxSkeleton from './xtx-skeleton.vue'
// 引入轮播图组件
import XtxCarousel from './xtx-carousel.vue'

// 更多内容组件
import XtxMore from './xtx-more.vue'
// 面包屑组件
import XtxBread from './xtx-bread.vue'
// 面包屑内层组件
import XtxBreadItem from './xtx-bread-item.vue'
// 多选框组件
import XtxCheckbox from './xtx-checkbox.vue'
// 加载更多组件
import XtxInfinteLoading from './xtx-infinite-loading.vue'
// 城市组件
import XtxCity from './xtx-city.vue'
// sku组件
import GoodsSku from './goods-sku.vue'
// 数量组件
import XtxNumbox from './library.vue'
// 按钮组件
import XtxButton from './xtx-button.vue'
// 分页组件
import XtxPagination from './xtx-pagination.vue'
// 弹窗提示信息组件
import XtxMessage from './xtx-message.vue'
// 确认提示框
import XtxConfirm from './xtx-confirm.vue'
// 添加地址组件
import XtxDialog from './xtx-dialog.vue'

const myPlugin = {
  install (app) {
    // app为vue的实例
    // app.component('组件名',组件对象）
    app.component(XtxSkeleton.name, XtxSkeleton)
    app.component(XtxCarousel.name, XtxCarousel)
    app.component(XtxMore.name, XtxMore)
    app.component(XtxBread.name, XtxBread)
    app.component(XtxBreadItem.name, XtxBreadItem)
    app.component(XtxCheckbox.name, XtxCheckbox)
    app.component(XtxInfinteLoading.name, XtxInfinteLoading)
    app.component(XtxCity.name, XtxCity)
    app.component(GoodsSku.name, GoodsSku)
    app.component(XtxNumbox.name, XtxNumbox)
    app.component(XtxButton.name, XtxButton)
    app.component(XtxPagination.name, XtxPagination)
    app.component(XtxMessage.name, XtxMessage)
    app.component(XtxConfirm.name, XtxConfirm)
    app.component(XtxDialog.name, XtxDialog)
  }
}

export default myPlugin
