import axios from 'axios'
import store from '@/store'
import router from '@/router'

const instance = axios.create({
  // baseURL: 'https://apipc-xiaotuxian-front.itheima.net',
  baseURL: 'http://pcapi-xiaotuxian-front-devtest.itheima.net/',
  timeout: 5000
})

// 请求拦截器
instance.interceptors.request.use(
  // config为当前的请求，会拦截到所有的请求
  config => {
    // do something before request is sent
    const token = store.state.user.profile.token
    if (token) {
      // 如果有token就在请求头中加入token
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// 响应拦截器
// 1.根据后端返回数据判断本次操作是否成功，不成功主动报错
// 2.如果成功，只返回有效数据
instance.interceptors.response.use(response => response.data, error => {
  if (error.response && error.response.status === 401) {
    // 1. 清空个人信息清空token
    store.commit('user/setUser', {})
    // store.dispatch('user/userLogout')
    // 2. 重新登录(vue3中的响应式数据需要加.value)
    router.push('/login?return_url=' + encodeURIComponent(router.currentRoute.value.fullPath))
  }
  return Promise.reject(error) // 返回执行错误 让当前的执行链跳出成功 直接进入 catch
})

export default function (url, method, paramsData) {
  return instance({
    url,
    method,
    // 此处为什么是中括号？中括号代表是动态赋值，用其他符号则不行
    [method.toLowerCase() === 'get' ? 'params' : 'data']: paramsData
  })
}
