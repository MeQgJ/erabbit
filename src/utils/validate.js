export default {
  // 以校验的属性命名
  mobile (value) {
    if (!value) return '请输入手机号'
    if (!/^1[3-9]\d{9}$/.test(value)) return '手机号格式错误'
    return true
  },
  account (value) {
    // return true通过；return '说明'不通过
    if (!value) return '用户名不能为空'
    return true
  },
  password (value) {
    if (!value) return '请输入密码'
    if (!/\d{6}$/.test(value)) return '密码格式错误'
    return true
  },
  rePassword (value, { form }) {
    if (!value) return '请输入密码'
    if (!/^\w{6,24}$/.test(value)) return '密码是6-24个字符'
    // 校验密码是否一致  form表单数据对象
    if (value !== form.password) return '两次输入的密码不一致'
    return true
  },
  code (value) {
    if (!value) return '请输入手机验证码'
    if (!/\d{6}$/.test(value)) return '验证码格式错误'
    return true
  },
  isAgree (value) {
    return value || '不答应可不行'
  }
}
