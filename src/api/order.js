/* eslint-disable no-tabs */
import request from '@/utils/request'

/**
 * 获取结算信息
 */
export const findCheckoutInfo = () => {
  return request('/member/order/pre', 'get')
}

/**
 * 添加收货地址信息
 * @param {Object} address - 地址对象
 */
export const addAddress = (address) => {
  return request('/member/address', 'post', address)
}

/**
 * 编辑收货地址信息
 * @param {Object} address - 地址对象
 */
export const editAddress = (address) => {
  return request('/member/address/' + address.id, 'put', address)
}

/**
 * 提交订单
 * @param {Object} order - 订单信息对象
  - goods	            object [{skuId,count}]	必须		商品集合
  - addressId	       string	必须		所选地址Id
  - deliveryTimeType	integer	必须		配送时间类型，1为不限，2为工作日，3为双休或假日
  -	payType	         integer	必须		支付方式，1为在线支付，2为货到付款
  -	payChannel	integer	必须	支付渠道，1支付宝、2微信(在线支付时传值，为货到付款时不传值)
  - buyerMessage	    string	买家留言
 */
export const createOrder = (order) => {
  return request('/member/order', 'post', order)
}

/**
 * 获取订单详情
 * @param {String} id - 订单ID
 */
export const findOrder = (id) => {
  return request('/member/order/' + id, 'get')
}

/**
 * 获取订单详细
 * @param {String} orderId - 订单ID
 * @returns
 */
export const findOrderDetail = (orderId) => {
  return request('/member/order/' + orderId, 'get')
}
