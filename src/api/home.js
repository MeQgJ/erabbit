// 定义首页需要的接口函数
import request from '@/utils/request'

/**
 * 获取首页头部分类数据
 */
export const findHeadCategory = () => {
  return request('/home/category/head', 'get')
}

// limit = 6参数的默认值
export const findBrand = (limit = 6) => {
  return request('/home/brand', 'get', { limit })
}

/**
 * 获取广告图
 * @returns Promise
 */
export const findBanner = () => {
  return request('/home/banner', 'get')
}

// 主页-发现好物
export const findNew = () => {
  return request('home/new', 'get')
}

// 主页 - 人气推荐
export const findHot = () => {
  return request('home/hot', 'get')
}

// 获取商品
export const findGoods = () => {
  return request('home/goods', 'get')
}

export const findSpecial = () => {
  return request('home/special', 'get')
}
