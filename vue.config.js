// 路径拼接
const path = require('path')
module.exports = {
  configureWebpack: {
    externals: {
      qc: 'QC' // qc这个包，不需要从node_modules中查找，因为引入了线上资源
    }
  },
  chainWebpack: config => {
    config.devServer.disableHostCheck(true)
    config.module
      .rule('images')
      .use('url-loader')
      .loader('url-loader')
      .tap(options => Object.assign(options, { limit: 10000 }))
  },
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'less',
      patterns: [
        path.join(__dirname, './src/styles/variables.less'),
        path.join(__dirname, './src/styles/mixins.less')
      ]
    }
  }
}
